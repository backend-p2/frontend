import axios from 'axios';

const API_URL = '/api';

const api = axios.create({
    baseURL: API_URL,
});

class API {

    static uploadTgz(sid, file) {
        const url = `/upload_tgz?filename=${file.name}&sid=${sid}`;
        return api.post(url, file)
            .then(({ status }) => status);
    }
}

export default API;
